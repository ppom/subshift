with import <nixpkgs> {};

let
in
pkgs.mkShell {
  name = "rust-tools";
  buildInputs = [
    cargo
    rustc
    rustfmt
    rustup
  ];
  src = null;
  shellHook = ''
  '';
}

