# Subshift

Currently, `subshift` is only able to *quickly* shift .srt subtitles.

*Shifting* subtitles is the only operation I used on other more complex tools:

- headless alternative to [GNOME Subtitle Editor](https://gnomesubtitles.org/), which is a full GUI.
- quicker alternative to [subedit](https://github.com/helixarch/subedit), which is written in Bash, and can take 20 seconds to shift a file.

So that's what's implemented, but other operations are easy to make, now that parsing from and dumping to .srt format is done.

## Examples

Running `subshift 00:00:20,000 file.srt` will add 20 seconds to every timestamp found in the file.

Running `subshift -1:2,3 file.srt` will remove 62,300 seconds to every timestamp found in the file.

## Timestamp format

The duration shift can be in a more flexible version of the actual .srt duration format:

Leading zeroes, hours and milliseconds can be omitted.
Minutes can be omitted if hours are also omitted.

## Building

You must have the Rust toolchain installed.
`subshift` uses no external dependencies, compiling in a very tiny executable.
```sh
cargo build --release
sudo cp ./target/release/subshift /usr/local/bin/
```
