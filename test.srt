1
00:00:38,820 --> 00:00:43,814
Ceci est une histoire simple,
qui n'est pas facile à raconter.

2
00:00:44,020 --> 00:00:46,898
Comme dans un conte
on y trouve du chagrin...

3
00:00:47,100 --> 00:00:50,649
mais aussi
des miracles et du bonheur.

4
00:00:53,540 --> 00:00:56,134
Je vois ce que je vois,
rien ne m'échappe.

5
00:00:56,340 --> 00:00:59,093
"'Me voici, Chaos.
Je suis votre esclave.'

6
00:00:59,300 --> 00:01:02,133
Et lui de répondre : 'Très bien !'

7
00:01:02,340 --> 00:01:05,252
Je suis enfin libre.
Que m'apporterait une caresse...

8
00:01:05,460 --> 00:01:08,497
si cette brise est mon dû ?

9
00:01:08,700 --> 00:01:10,452
Me voici.

10
00:01:10,660 --> 00:01:13,049
Plus de trains, et plus de freins.

11
00:01:13,260 --> 00:01:16,889
Et je ne peux vous résister.
Bacchus, emmène-moi..."

12
00:01:17,100 --> 00:01:20,570
- On n'a plus de freins !
- Je t'ai entendu.

13
00:01:20,780 --> 00:01:24,978
- On n'a vraiment plus de freins !
- C'était un poème, non ?

14
00:01:25,180 --> 00:01:27,136
Freine !

15
00:01:50,260 --> 00:01:54,333
- Le roi arrive.
- Le voilà.

16
00:01:55,780 --> 00:02:00,012
- Il y a là une foule.
- Fonce tout droit !

17
00:02:00,220 --> 00:02:04,179
On n'a plus de freins ! Écartez-vous !

18
00:02:13,740 --> 00:02:21,818
LA VIE EST BELLE

19
00:02:29,740 --> 00:02:33,619
Va faire un tour.
Ou nous serons encore ici demain.

20
00:02:33,820 --> 00:02:37,017
- J'ai trouvé le vis.
- Merci.

21
00:02:37,220 --> 00:02:41,099
- Qu'est-ce qu'il te faut ?
- Rien. Fiche-moi la paix 10 minutes.

22
00:02:44,620 --> 00:02:48,977
- Tu veux la vis ?
- Non, je veux la paix.

23
00:02:49,180 --> 00:02:50,579
C'est bon.

24
00:03:00,140 --> 00:03:05,612
- Je jette la vis si je la trouve ?
- Non, donne-moi 10 minutes.

25
00:03:06,940 --> 00:03:09,090
Je vais me laver les mains.

26
00:03:15,780 --> 00:03:20,535
Jolie fille ! Comment ça va ?
Que fais-tu là ?

27
00:03:20,740 --> 00:03:25,097
THE END ;)

