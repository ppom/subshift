use std::time::Duration;
use std::vec::Vec;

use crate::error::SubError;
use crate::srt::{read_srt, write_srt, Subtitle};

/// A struct implementing this trait represents an operation supported by the CLI.
///
/// Operations are self-contained on embed all their parameters.
pub trait Operate {
    fn operate(&self) -> Result<(), SubError>;
}

#[derive(Default)]
pub struct Help;

pub struct Shift {
    pub filename: String,
    pub duration: Duration,
    pub positive: bool,
    pub from: Option<Duration>,
    pub to: Option<Duration>,
}

impl Operate for Help {
    fn operate(&self) -> Result<(), SubError> {
        println!(
            "subshift: shift subtitles time
Usage:
subshift help
subshift shift [ --from TIME ] [ --to TIME ] [+/-]TIME FILE

-f/--from: shift subtitles only from this starting time
-t/--to:   shift subtitles only until this ending  time

TIME: [[h:]m:]s[,c]

"
        );
        Ok(())
    }
}

impl Default for Shift {
    fn default() -> Self {
        Shift {
            filename: String::default(),
            duration: Duration::default(),
            positive: true,
            from: None,
            to: None,
        }
    }
}

impl Shift {
    fn _shift(&self, subtitles: Vec<Subtitle>) -> Result<Vec<Subtitle>, SubError> {
        let new_from = match self.from {
            Some(f) => f,
            None => Duration::default(), // zero
        };
        let new_to = match self.to {
            Some(t) => t,
            None => Duration::MAX,
        };
        Ok(subtitles
            .iter()
            .map(|subtitle| {
                // Note we only care about beginning times
                if subtitle.begin >= new_from && subtitle.begin <= new_to {
                    subtitle.shift(self.positive, self.duration)
                } else {
                    subtitle.clone()
                }
            })
            .collect())
    }
}

impl Operate for Shift {
    fn operate(&self) -> Result<(), SubError> {
        match read_srt(&self.filename) {
            Err(e) => Err(e),
            Ok(subtitles) => match self._shift(subtitles) {
                Ok(shifted_subtitles) => write_srt(&self.filename, shifted_subtitles),
                Err(e) => Err(e),
            },
        }
    }
}
