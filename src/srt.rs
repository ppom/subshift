use std::fs::File;
use std::fs::OpenOptions;
use std::io::BufRead;
use std::io::BufReader;
use std::io::BufWriter;
use std::io::Write;
use std::time::Duration;
use std::vec::Vec;

use crate::duration::srt_fmt;
use crate::duration::SRTDuration;
use crate::error::SubError;

/// A .srt file is a list of subtitles that contain
/// - a beginning time
/// - a ending time
/// - a text
///
/// This struct is the building block of its in-memory representation.
///
#[derive(Debug, Clone)]
pub struct Subtitle {
    pub begin: Duration,
    pub end: Duration,
    pub text: String,
}

impl Subtitle {
    /// Returns a new Subtitle with its beginning and ending times *shifted* to a specified
    /// Duration.
    ///
    /// If positive is true, time will be added, if false, time will be substracted.
    ///
    pub fn shift(&self, positive: bool, duration: Duration) -> Subtitle {
        if positive {
            Subtitle {
                begin: self.begin.saturating_add(duration),
                end: self.end.saturating_add(duration),
                text: self.text.clone(),
                // TODO test ..self instead of self.text.clone()
                // TODO + instead of saturating_add?
            }
        } else {
            Subtitle {
                begin: self.begin.saturating_sub(duration),
                end: self.end.saturating_sub(duration),
                text: self.text.clone(),
            }
        }
    }
}

enum SRTReadState {
    HeaderRead,
    TimeRead,
    ReadingText,
    SplitRead,
    Start,
}

/// Parses an opened .srt file
/// This is where most of the parsing logic is.
fn parse_srt(file: &File) -> Result<Vec<Subtitle>, SubError> {
    let reader = BufReader::new(file);

    let mut line_count = 0;
    let mut last_sub_count = 0;
    let mut subtitles = Vec::new();
    let mut state = SRTReadState::Start;
    let mut current_begin: Option<Duration> = None;
    let mut current_end: Option<Duration> = None;
    let mut current_text: Option<String> = None;

    for line in reader.lines() {
        match line {
            Err(_) => {
                return Err(SubError::SubParseError(format!(
                    "{}: decode error",
                    line_count
                )))
            }
            Ok(line) => {
                line_count += 1;
                match state {
                    SRTReadState::Start | SRTReadState::SplitRead => {
                        // This may be a header or a split
                        if !line.is_empty() {
                            // Parse header
                            let new_sub_count: u32 = match line.parse() {
                                Ok(number) => number,
                                Err(_) => {
                                    return Err(SubError::SubParseError(format!(
                                        "{} : line should be a number",
                                        line_count
                                    )))
                                }
                            };
                            if new_sub_count != last_sub_count + 1 {
                                println!(
                                    "Warning, line {}: jumping from sub {} to sub {}",
                                    line_count, last_sub_count, new_sub_count,
                                );
                            }
                            last_sub_count = new_sub_count;
                            state = SRTReadState::HeaderRead;
                        }
                        // Split => No-op
                    }
                    SRTReadState::HeaderRead => {
                        // This must be times

                        match parse_times(&line) {
                            Ok((begin, end)) => {
                                current_begin = Some(begin);
                                current_end = Some(end);
                            }
                            Err(msg) => {
                                return Err(SubError::SubParseError(format!(
                                    "{}: {}",
                                    line_count, msg
                                )))
                            }
                        }
                        state = SRTReadState::TimeRead;
                    }
                    SRTReadState::TimeRead => {
                        // This must be text
                        if line.is_empty() {
                            // Error
                            return Err(SubError::SubParseError(format!(
                                "{}: line should not be empty",
                                line_count
                            )));
                        }
                        // Add text
                        current_text = Some(line + "\n");
                        state = SRTReadState::ReadingText;
                    }
                    SRTReadState::ReadingText => {
                        // This may be text or a split
                        if !line.is_empty() {
                            // Add text
                            let mut new_text = current_text.unwrap();
                            new_text.push_str(&line);
                            new_text.push('\n');
                            current_text = Some(new_text);
                            // Stay in ReadingText state
                        } else {
                            // Add subtitle
                            subtitles.push(Subtitle {
                                begin: current_begin.unwrap(),
                                end: current_end.unwrap(),
                                text: current_text.unwrap(),
                            });
                            state = SRTReadState::SplitRead;
                            current_text = None;
                        }
                    }
                }
            }
        }
    }
    match state {
        SRTReadState::ReadingText => {
            subtitles.push(Subtitle {
                begin: current_begin.unwrap(),
                end: current_end.unwrap(),
                text: current_text.unwrap(),
            });
        }
        SRTReadState::SplitRead => {}
        _other => {
            return Err(SubError::SubParseError(format!(
                "{}: file truncated at the end",
                line_count
            )))
        }
    }
    Ok(subtitles)
}

/// Parses a .srt "duration" line
///
/// # Example line:
/// 00:00:38,820 --> 00:00:43,814
///
fn parse_times(line: &str) -> Result<(Duration, Duration), String> {
    let mut times = line.split(" --> ");
    let current_begin: Option<Duration>;
    let current_end: Option<Duration>;
    match times.next() {
        Some(time) => match Duration::from_srt(time) {
            Ok(begin) => current_begin = Some(begin),
            Err(_) => return Err("Unable to parse beginning of subtitle".to_string()),
        },
        None => return Err("subtitle parts missing".to_string()),
    }
    match times.next() {
        Some(time) => match Duration::from_srt(time) {
            Ok(end) => current_end = Some(end),
            Err(_) => return Err("Unable to parse end of subtitle".to_string()),
        },
        None => return Err("end part of subtitle missing".to_string()),
    }
    if times.next().is_some() {
        return Err("Unable to parse subtitle".to_string());
    }
    Ok((current_begin.unwrap(), current_end.unwrap()))
}

/// Open a file and tries to parse its contents into a Vec<Subtitle>
///
pub fn read_srt(filename: &str) -> Result<Vec<Subtitle>, SubError> {
    match OpenOptions::new().read(true).open(&filename) {
        Err(e) => Err(SubError::IoError(e)),
        Ok(file) => Ok(parse_srt(&file)?),
    }
}

/// Tries to dump a Vec<Subtitle> to a path
///
/// The path may exist or not.
pub fn write_srt(path: &str, subtitles: Vec<Subtitle>) -> Result<(), SubError> {
    let mut buffer = BufWriter::new(OpenOptions::new().write(true).truncate(true).open(&path)?);
    for (i, subtitle) in subtitles.iter().enumerate() {
        writeln!(buffer, "{}", i + 1)?;
        writeln!(
            buffer,
            "{} --> {}",
            srt_fmt(&subtitle.begin),
            srt_fmt(&subtitle.end)
        )?;
        writeln!(buffer, "{}", &subtitle.text)?;
    }
    Ok(())
}
