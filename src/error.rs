use std::convert::From;
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::io;

use crate::duration::SRTDurationConversionError;

/// Sub Error, used inside this module
#[derive(Debug)]
pub enum SubError {
    ParseError(String),
    SubParseError(String),
    FileDoesNotExist,
    WrongTime,
    ZeroTime,
    IoError(io::Error),
    ParseIntError(std::num::ParseIntError),
}

impl Display for SubError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                SubError::ParseError(err) => format!("Argument non reconnu: {}", err),
                SubError::SubParseError(lineno) =>
                    format!("Erreur de syntaxe à la ligne {}", lineno),
                SubError::FileDoesNotExist => "Le fichier n'existe pas".into(),
                SubError::WrongTime => "Le temps donné n'est pas compris".into(),
                SubError::ZeroTime => "Le temps donné est nul, il n'y a rien à faire".into(),
                SubError::IoError(ie) => format!("Erreur d'entrée/sortie: {}", ie),
                SubError::ParseIntError(pie) => format!("Erreur de conversion: {}", pie),
            }
        )
    }
}

impl Error for SubError {}

impl From<io::Error> for SubError {
    fn from(error: io::Error) -> Self {
        SubError::IoError(error)
    }
}

impl From<std::num::ParseIntError> for SubError {
    fn from(error: std::num::ParseIntError) -> Self {
        SubError::ParseIntError(error)
    }
}

impl From<SRTDurationConversionError> for SubError {
    fn from(_error: SRTDurationConversionError) -> Self {
        SubError::WrongTime
    }
}
