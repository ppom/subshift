use std::env;
use std::path::Path;
use std::time::Duration;

mod duration;
mod error;
mod operate;
mod srt;

use duration::SRTDuration;
use error::SubError;
use operate::{Help, Operate, Shift};

fn main() -> Result<(), u8> {
    match parse_args(Box::new(env::args())) {
        Ok(operation) => match operation.operate() {
            Ok(()) => Ok(()),
            Err(err) => {
                eprintln!("{}", err);
                Err(3)
            }
        },
        Err(err) => {
            eprintln!("{}\n", err);
            Help::default().operate().unwrap();
            Err(1)
        }
    }
}

/// Parses command line to create a `dyn Operate`
///
fn parse_args(mut args: Box<dyn Iterator<Item = String>>) -> Result<Box<dyn Operate>, SubError> {
    const FIRST_ERROR: &str = "First argument must be `help` or `shift`";
    args.next(); // first arg is executable name

    match args.next() {
        Some(arg) => {
            let mut duration_done = false;
            let mut file_done = false;

            match arg.as_str() {
                "-h" | "--help" | "help" => Ok(Box::new(Help::default())),
                "shift" => {
                    let mut shift = Shift::default();
                    while let Some(arg) = args.next() {
                        match arg.as_str() {
                            "-f" | "--from" => {
                                shift.from = match args.next() {
                                    Some(number) => Some(Duration::from_srt(&number)?),
                                    None => {
                                        return Err(SubError::ParseError(
                                            "duration missing after --from".to_string(),
                                        ))
                                    }
                                };
                            }

                            "-t" | "--to" => {
                                shift.to = match args.next() {
                                    Some(number) => Some(Duration::from_srt(&number)?),
                                    None => {
                                        return Err(SubError::ParseError(
                                            "duration missing after --to".to_string(),
                                        ))
                                    }
                                };
                            }

                            arg => match duration_done {
                                // We expect a duration
                                false => {
                                    let negative = arg.starts_with('-');
                                    let number_str = match negative {
                                        true => {
                                            shift.positive = false;
                                            match arg.get(1..) {
                                                Some(slice) => slice,
                                                None => return Err(SubError::WrongTime),
                                            }
                                        }
                                        false => arg,
                                    };
                                    shift.duration = Duration::from_srt(number_str)?;
                                    if shift.duration.is_zero() {
                                        return Err(SubError::ZeroTime);
                                    }
                                    duration_done = true;
                                }

                                // We expect a filename
                                true => {
                                    if !Path::new(arg).exists() {
                                        return Err(SubError::FileDoesNotExist);
                                    }
                                    shift.filename = arg.to_string();
                                    file_done = true;
                                }
                            },
                        }
                    }
                    if !duration_done {
                        return Err(SubError::ParseError("duration missing".to_string()));
                    }
                    if !file_done {
                        return Err(SubError::ParseError("filename missing".to_string()));
                    }
                    Ok(Box::new(shift))
                }
                _ => Err(SubError::ParseError(String::from(FIRST_ERROR))),
            }
        }
        None => Err(SubError::ParseError(String::from(FIRST_ERROR))),
    }
}

#[cfg(test)]
mod tests {
    use crate::{parse_args, SubError};

    fn vec2iter(v: &'static [&'static str]) -> Box<dyn Iterator<Item = String>> {
        Box::new(v.into_iter().map(|s| s.to_string()))
    }

    #[test]
    fn parse_empty_commandline() {
        let args = vec2iter(&["subshift"]);
        match parse_args(args) {
            Ok(_) => {
                panic!("subshift should not execute with no arguments")
            }
            Err(e) => {
                assert!(matches!(e, SubError::ParseError(_)))
            }
        }
    }

    #[test]
    fn parse_help() {
        let args = vec2iter(&["subshift", "--help"]);
        parse_args(args).unwrap();
        let args = vec2iter(&["subshift", "-h"]);
        parse_args(args).unwrap();
    }

    #[test]
    fn parse_shift_empty() {
        let args = vec2iter(&["subshift", "shift"]);
        match parse_args(args) {
            Ok(_) => {
                panic!("shift operation should not execute with no arguments")
            }
            Err(e) => {
                assert!(matches!(e, SubError::ParseError(_)))
            }
        }
    }

    #[test]
    fn parse_shift_wrong_arg() {
        let args = vec2iter(&["subshift", "shift", "test.srt"]);
        match parse_args(args) {
            Ok(_) => {
                panic!("shift operation should not execute with wrong arguments")
            }
            Err(e) => {
                assert!(matches!(e, SubError::WrongTime))
            }
        }
        let args = vec2iter(&["subshift", "shift", "00:00:20,000"]);
        match parse_args(args) {
            Ok(_) => {
                panic!("shift operation should not execute with wrong arguments")
            }
            Err(e) => {
                assert!(matches!(e, SubError::ParseError(_)))
            }
        }
        let args = vec2iter(&["subshift", "shift", "00:00:20,000", "not_existing_file.srt"]);
        match parse_args(args) {
            Ok(_) => {
                panic!("shift operation should not execute with wrong arguments")
            }
            Err(e) => {
                assert!(matches!(e, SubError::FileDoesNotExist))
            }
        }
    }

    #[test]
    fn parse_shift_valids_arg() {
        let args = vec2iter(&["subshift", "shift", "00:00:20,000", "test.srt"]);
        parse_args(args).unwrap();
        let args = vec2iter(&[
            "subshift",
            "shift",
            "--from",
            "00:00:02,000",
            "00:00:20,000",
            "test.srt",
        ]);
        parse_args(args).unwrap();
        let args = vec2iter(&[
            "subshift",
            "shift",
            "--from",
            "00:00:02,000",
            "--to",
            "00:00:10,000",
            "00:00:20,000",
            "test.srt",
        ]);
        parse_args(args).unwrap();
    }
}
