use std::time::Duration;

pub struct SRTDurationConversionError;

pub trait SRTDuration {
    fn from_srt(srt: &str) -> Result<Duration, SRTDurationConversionError>;
}

impl SRTDuration for Duration {
    // [[h:]m:]s[,c]
    fn from_srt(srt: &str) -> Result<Duration, SRTDurationConversionError> {
        let mut hours = 0;
        let mut minutes = 0;
        let mut seconds = 0;
        let mut milliseconds = 0;

        let mut number_of_colons = 0;
        let mut current_digits = String::from("");

        for c in (srt.to_owned() + "\0").chars() {
            if c.is_numeric() {
                current_digits.push(c);
            } else if c == ':' || c == ',' || c == '\0' {
                let current_number = match current_digits.parse() {
                    Ok(ok) => ok,
                    Err(_) => return Err(SRTDurationConversionError),
                };
                match number_of_colons {
                    0 => seconds = current_number,
                    1 => {
                        minutes = seconds;
                        seconds = current_number;
                    }
                    2 => {
                        if c == ':' {
                            return Err(SRTDurationConversionError);
                        }
                        hours = minutes;
                        minutes = seconds;
                        seconds = current_number;
                    }
                    3 => {
                        if c != '\0' {
                            return Err(SRTDurationConversionError);
                        }
                        milliseconds = match current_digits.len() {
                            1 => current_number * 100,
                            2 => current_number * 10,
                            3 => current_number,
                            _other => return Err(SRTDurationConversionError),
                        };
                    }
                    _more => return Err(SRTDurationConversionError),
                }
                number_of_colons = match c {
                    ':' => number_of_colons + 1,
                    ',' => 3,
                    '\0' => 4, // unused
                    _impossible => 4,
                };
                current_digits.clear();
            }
        }
        Ok(Duration::from_millis(
            milliseconds + 1000 * seconds + 1000 * 60 * minutes + 1000 * 60 * 60 * hours,
        ))
    }
}

pub fn srt_fmt(duration: &Duration) -> String {
    let millis = duration.as_millis();
    format!(
        "{:02}:{:02}:{:02},{:03}",
        millis / (1000 * 60 * 60),
        (millis / (1000 * 60)) % 60,
        (millis / 1000) % 60,
        millis % 1000
    )
}
